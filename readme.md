# YouTube Search And Favorites

Searching on YouTube with app. You can watch them later and check/uncheck favorite.

## Getting Started

Download this app from: [LINK] or [LINK2]
And install this with your Android phone.

### Prerequisites

Minimum Android 4.1.2 (JellyBean - API 16)

### Installing

After downloading the app from [LINK] or [LINK2] install them with your Android phone.
Open a file explorer (example.: Total Commander) search the app .apk file and click.

If your device asks you: "Do you trust the developer, source?" (Or other message :D)
You answer yes.

And now installing the app.

If this process is completed: LET'S GO ! :D

## Built with

Ap created with [Android Studio 3.1.1](https://www.google.com)

App icons edited with GIMP: [GIMP 2.8.22](https://www.gimp.org/)

Favorite, play, search icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [Flaticon](www.flaticon.com) is licensed by [Creative Commons 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

YouTube icon made by [Freepik](https://www.freepik.com/) from [Flaticon](www.flaticon.com) is licensed by [Creative Commons 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

Close icon made by [RobinKylander](https://www.flaticon.com/authors/robin-kylander) from [Flaticon](www.flaticon.com) is licensed by [Creative Commons 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

## Author(s)

Dancs Bertalan

## Versioning

0.1b - beta

## License

[Creative Commons 3.0](http://creativecommons.org/licenses/by/3.0/)


## Acknowledgments

* Thanks for the inspirations,task for [VividMind zrt](www.vividmindsoft.com)
